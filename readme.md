# Chrome headless custom

lazy to testing...  
auto saving 375px, 768px, 1280px screenshot.

## Reference 

Getting Started with Headless Chrome  
https://developers.google.com/web/updates/2017/04/headless-chrome  


cyrus-and/chrome-remote-interface  
https://github.com/cyrus-and/chrome-remote-interface  
https://github.com/cyrus-and/chrome-remote-interface/wiki


## Steps

```
$ npm install
```

### Step1 Starting Remote debugging

```
$ /Applications/Google\ Chrome.app/Contents/MacOS/Google\ Chrome --headless --remote-debugging-port=9222 --disable-gpu    
```

### Step2 Get ScreenShot
```
$ node screenshot.js --url https://gitlab.com/NickChen14/chrome-headless-custom --fullPage true 
```
